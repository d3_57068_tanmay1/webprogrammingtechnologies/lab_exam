const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.post("/signin", (request, response) => {
  const { email, password } = request.body;

  const connection = db.openConnection();

  const emailStatement = `Select empid,companyname from usertb Where email='${email}' and password='${password}'`;
  connection.query(emailStatement, (error, emps) => {
    connection.end();
    if (error) response.send(utils.createResult(error));
    else if (emps.length == 0)
      response.send(utils.createResult("user not found"));
    else {
      const user = emps[0];
      response.send(utils.createResult(null, user));
    }
  });
});

module.exports = router;
