const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.post("/add", (request, response) => {
  const { id, name, model, price, color } = request.body;
  const connection = db.openConnection();
  const statement = `insert into cartb values(${id},'${name}','${model}','${price}','${color}')`;
  connection.query(statement, (error, result) => {
    connection.end();
    if (error) response.send(utils.createResult(error));
    else {
      response.send(utils.createResult(null, result));
    }
  });
});

router.get("/", (request, response) => {
  const connection = db.openConnection();
  const statement = `select id,name,model,price,carColor From cartb`;
  connection.query(statement, (error, result) => {
    connection.end();
    if (error) response.send(utils.createResult(error));
    else {
      response.send(utils.createResult(null, result));
    }
  });
});

router.delete("/delete/:id", (request, response) => {
  const { id } = request.params;
  const connection = db.openConnection();
  console.log(id);
  const statement = `delete from cartb where id='${id}'`;
  connection.query(statement, (error, result) => {
    connection.end();
    if (error) response.send(utils.createResult(error));
    else {
      response.send(utils.createResult(null, result));
    }
  });
});

module.exports = router;
