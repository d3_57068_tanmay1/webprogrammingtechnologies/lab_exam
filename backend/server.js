const express = require("express");
const cors = require("cors");
const routerEmp = require("./routes/emp");
const routerCar = require("./routes/car");

const app = express();

app.use(cors("*"));
app.use(express.json());

app.use("/emp", routerEmp);
app.use("/car", routerCar);

app.listen(4000, () => {
  console.log("Server Started on port 4000");
});
