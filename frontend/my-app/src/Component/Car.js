const Car = (props) => {
  const { cars } = props;

  return (
    <div>
      <h1> {cars.id} </h1>
      <h1> {cars.model} </h1>
      <h1> {cars.name} </h1>
      <h1> {cars.price} </h1>
      <h1> {cars.carColor} </h1>
      <hr />
    </div>
  );
};

export default Car;
