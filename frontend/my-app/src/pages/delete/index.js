import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";

const Delete = () => {
  const [id, setId] = useState(0);
  const navigate = useNavigate();

  const deleteCar = () => {
    if (id == 0) toast.warning("Please Enter id");

    const url = `http://localhost:4000/car/delete/${id}`;
    axios.delete(url).then((response) => {
      const result = response.data;
      console.log(result);
      if (result["status"] == "success") {
        toast.success("Successfully Deleted");
        navigate("/home");
      } else toast.error("No car found with this id");
    });
  };

  return (
    <div>
      <h1>Delete Page</h1>
      <div className="container">
        <div className="row align-items-start">
          <div className="col"></div>
          <div className="col">
            <div className="mb-3">
              <label for="id" className="form-label">
                Id:
              </label>
              <input
                onChange={(e) => {
                  setId(e.target.value);
                }}
                id="id"
                type="number"
                className="form-control"
              />
            </div>
            <button
              onClick={deleteCar}
              type="submit"
              className="btn btn-primary"
            >
              Delete
            </button>
          </div>
          <div className="col"></div>
        </div>
      </div>
    </div>
  );
};

export default Delete;
