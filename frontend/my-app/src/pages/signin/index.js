import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import "./index.css";

const Signin = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const signinuser = () => {
    if (email === 0) toast.warning("Please Enter Email");
    else if (password === 0) toast.warning("Please Enter password");
    else {
      const body = {
        email,
        password,
      };
      const url = `http://localhost:4000/emp/signin`;
      axios.post(url, body).then((response) => {
        const result = response.data;

        if (result["status"] === "success") {
          toast.success("Welcome to Application");
          const { empid, companyname } = result["data"];

          sessionStorage["empid"] = empid;
          sessionStorage["companyname"] = companyname;
          navigate("/add");
        } else toast.error("Invalid Username or password");
      });
    }
  };

  return (
    <div>
      <h1>SignIn</h1>
      <div className="row">
        <div className="col"></div>
        <div className="col">
          <div className="form">
            <div className="mb-3">
              <label className="form-label">Email ID :</label>
              <input
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                type="text"
                className="form-control"
              />

              <label className="form-label">Password :</label>
              <input
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                type="password"
                className="form-control"
              />

              <button onClick={signinuser} className="btn btn-primary">
                Sign In
              </button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
};
export default Signin;
