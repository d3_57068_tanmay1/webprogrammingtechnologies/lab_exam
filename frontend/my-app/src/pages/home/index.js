import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Car from "../../Component/Car";
import "./index.css";

const Home = () => {
  const [res, setResult] = useState([]);

  const GetCars = () => {
    const url = "http://localhost:4000/car/";

    axios.get(url).then((response) => {
      const res = response.data;
      setResult(res.data);
    });
  };

  useEffect(() => {
    GetCars();
  }, []);

  console.log(res);
  return (
    <div>
      home
      {res.map((car) => {
        return <Car cars={car}></Car>;
      })}
    </div>
  );
};

export default Home;
