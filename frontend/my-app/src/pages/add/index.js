import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import "./index.css";

const Add = () => {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [model, setModel] = useState("");
  const [price, setPrice] = useState("");
  const [color, setColor] = useState("");

  const navigate = useNavigate();

  const addCar = () => {
    if (id.length == 0) {
      toast.warning("Please Enter id");
      console.log("test1");
    } else if (name.length == 0) {
      toast.warning("Please Enter name");
      console.log("test2");
    } else if (model.length == 0) {
      toast.warning("Please Enter model");
      console.log("test3");
    } else if (price.length == 0) {
      toast.warning("Please Enter price");
      console.log("test4");
    } else if (color.length == 0) {
      toast.warning("Please Enter color");
      console.log("test5");
    } else {
      const body = {
        id,
        name,
        model,
        price,
        color,
      };
      const url = `http://localhost:4000/car/add`;
      axios.post(url, body).then((response) => {
        const result = response.data;
        if (result["status"] == "success") {
          toast.success("Successfully Added");
          navigate("/home");
        } else toast.error("Invalid data entered");
      });
    }
  };

  return (
    <div>
      <h1>Add Car</h1>
      <div className="container">
        <div className="row align-items-start">
          <div className="col"></div>
          <div className="col">
            <div className="mb-3">
              <label htmlFor="id" className="form-label">
                Id:
              </label>
              <input
                onChange={(e) => {
                  setId(e.target.value);
                }}
                id="id"
                type="number"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Name Car :
              </label>
              <input
                onChange={(e) => {
                  setName(e.target.value);
                }}
                id="name"
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="model" className="form-label">
                Model :
              </label>
              <input
                onChange={(e) => {
                  setModel(e.target.value);
                }}
                id="model"
                type="text"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="price" className="form-label">
                Price :
              </label>
              <input
                onChange={(e) => {
                  setPrice(e.target.value);
                }}
                id="price"
                type="number"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="color" className="form-label">
                Color:
              </label>
              <input
                onChange={(e) => {
                  setColor(e.target.value);
                }}
                id="color"
                type="text"
                className="form-control"
              />
            </div>

            <button onClick={addCar} type="submit" className="btn btn-primary">
              Add
            </button>
          </div>
          <div className="col"></div>
        </div>
      </div>
    </div>
  );
};

export default Add;
